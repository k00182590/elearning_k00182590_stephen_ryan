﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import interfaces.IState;
	
	public class Play extends Sprite implements IState
	{
		public var game:Game;
		private var background:Background;
		private var a_Btn : A_Btn;
		private var b_Btn : B_Btn;
		private var c_Btn : C_Btn;
		private var d_Btn : D_Btn;
		private var e_Btn : E_Btn;
		private var f_Btn : F_Btn;
		private var g_Btn : G_Btn;
		private var h_Btn : H_Btn;
		private var i_Btn : I_Btn;
		private var j_Btn : J_Btn;
		private var k_Btn : K_Btn;
		private var l_Btn : L_Btn;
		private var m_Btn : M_Btn;
		private var n_Btn : N_Btn;
		private var o_Btn : O_Btn;
		private var p_Btn : P_Btn;
		private var q_Btn : Q_Btn;
		private var r_Btn : R_Btn;
		private var s_Btn : S_Btn;
		private var t_Btn : T_Btn;
		private var u_Btn : U_Btn;
		private var v_Btn : V_Btn;
		private var w_Btn : W_Btn;
		private var x_Btn : X_Btn;
		private var y_Btn : Y_Btn;
		private var z_Btn : Z_Btn;
		
		
		public function Play(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.GAMEPLAY);
			addChild(background);

			setupButtons();
		}
		
		private function onGameOver(event:Event):void
		{
		}
		private function setupButtons():void{
			
			a_Btn= new A_Btn();
			b_Btn= new B_Btn();
			c_Btn= new C_Btn();
			d_Btn= new D_Btn();
			e_Btn= new E_Btn();
			f_Btn= new F_Btn();
			g_Btn= new G_Btn();
			h_Btn= new H_Btn();
			i_Btn= new I_Btn();
			j_Btn= new J_Btn();
			k_Btn= new K_Btn();
			l_Btn= new L_Btn();
			m_Btn= new M_Btn();
			n_Btn= new N_Btn();
			o_Btn= new O_Btn();
			p_Btn= new P_Btn();
			q_Btn= new Q_Btn();
			r_Btn= new R_Btn();
			s_Btn= new S_Btn();
			t_Btn= new T_Btn();
			u_Btn= new U_Btn();
			v_Btn= new V_Btn();
			w_Btn= new W_Btn();
			x_Btn= new X_Btn();
			y_Btn= new Y_Btn();
			z_Btn= new Z_Btn();
			
			var i :int=0;
				
			q_Btn.x=650 + (100*i);
			q_Btn.y=620;
			addChild(q_Btn);
			i++
			
			w_Btn.x=650 + (100*i);
			w_Btn.y=618;
			addChild(w_Btn);
			i++
			
			e_Btn.x=650 + (100*i);
			e_Btn.y=620;
			addChild(e_Btn);
			i++
			
			r_Btn.x=650 + (100*i);
			r_Btn.y=620;
			addChild(r_Btn);
			i++
			
			t_Btn.x=650 + (100*i);
			t_Btn.y=620;
			addChild(t_Btn);
			i++
			
			y_Btn.x=650 + (100*i);
			y_Btn.y=617;
			addChild(y_Btn);
			i++
			
			u_Btn.x=650 + (100*i);
			u_Btn.y=620;
			addChild(u_Btn);
			i++
			
			i_Btn.x=650 + (100*i);
			i_Btn.y=618;
			addChild(i_Btn);
			i++
			
			o_Btn.x=650 + (100*i);
			o_Btn.y=620;
			addChild(o_Btn);
			i++
			
			p_Btn.x=650 + (100*i);
			p_Btn.y=620;
			addChild(p_Btn);
			i++
			
			i=0;
			a_Btn.x=698 + (100*i);
			a_Btn.y=710;
			addChild(a_Btn);
			i++
			
			s_Btn.x=698 + (100*i);
			s_Btn.y=710;
			addChild(s_Btn);
			i++
			
			d_Btn.x=698 + (100*i);
			d_Btn.y=710;
			addChild(d_Btn);
			i++
			
			f_Btn.x=698 + (100*i);
			f_Btn.y=710;
			addChild(f_Btn);
			i++
			
			g_Btn.x=698 + (100*i);
			g_Btn.y=710;
			addChild(g_Btn);
			i++
			
			h_Btn.x=698 + (100*i);
			h_Btn.y=710;
			addChild(h_Btn);
			i++
			
			j_Btn.x=698 + (100*i);
			j_Btn.y=710;
			addChild(j_Btn);
			i++
			
			k_Btn.x=698 + (100*i);
			k_Btn.y=710;
			addChild(k_Btn);
			i++
			
			l_Btn.x=698 + (100*i);
			l_Btn.y=710;
			addChild(l_Btn);
			i++
			
			i=0;
			z_Btn.x=748 + (100*i);
			z_Btn.y=780;
			addChild(z_Btn);
			i++
			
			x_Btn.x=748 + (100*i);
			x_Btn.y=780;
			addChild(x_Btn);
			i++
			
			c_Btn.x=748 + (100*i);
			c_Btn.y=790;
			addChild(c_Btn);
			i++
			
			v_Btn.x=748 + (100*i);
			v_Btn.y=790;
			addChild(v_Btn);
			i++
			
			b_Btn.x=748 + (100*i);
			b_Btn.y=790;
			addChild(b_Btn);
			i++
			
			n_Btn.x=748 + (100*i);
			n_Btn.y=790;
			addChild(n_Btn);
			i++
			
			m_Btn.x=748 + (100*i);
			m_Btn.y=790;
			addChild(m_Btn);
			i++
			
			q_Btn.addEventListener(MouseEvent.CLICK,onQ);
			w_Btn.addEventListener(MouseEvent.CLICK,onW);
			e_Btn.addEventListener(MouseEvent.CLICK,onE);
			r_Btn.addEventListener(MouseEvent.CLICK,onR);
			t_Btn.addEventListener(MouseEvent.CLICK,onT);
			y_Btn.addEventListener(MouseEvent.CLICK,onY);
			u_Btn.addEventListener(MouseEvent.CLICK,onU);
			i_Btn.addEventListener(MouseEvent.CLICK,onI);
			o_Btn.addEventListener(MouseEvent.CLICK,onO);
			p_Btn.addEventListener(MouseEvent.CLICK,onP);
			a_Btn.addEventListener(MouseEvent.CLICK,onA);
			s_Btn.addEventListener(MouseEvent.CLICK,onS);
			d_Btn.addEventListener(MouseEvent.CLICK,onD);
			f_Btn.addEventListener(MouseEvent.CLICK,onF);
			g_Btn.addEventListener(MouseEvent.CLICK,onG);
			h_Btn.addEventListener(MouseEvent.CLICK,onH);
			j_Btn.addEventListener(MouseEvent.CLICK,onJ);
			k_Btn.addEventListener(MouseEvent.CLICK,onK);
			l_Btn.addEventListener(MouseEvent.CLICK,onL);
			z_Btn.addEventListener(MouseEvent.CLICK,onZ);
			x_Btn.addEventListener(MouseEvent.CLICK,onX);
			c_Btn.addEventListener(MouseEvent.CLICK,onC);
			v_Btn.addEventListener(MouseEvent.CLICK,onV);
			b_Btn.addEventListener(MouseEvent.CLICK,onB);
			n_Btn.addEventListener(MouseEvent.CLICK,onN);
			m_Btn.addEventListener(MouseEvent.CLICK,onM);
	
		}
		
		public function update():void
		{
			background.update();
			
		}
		
		public function onQ(){
			textField.text = textField.text+="q";
		}
		
		public function onW(){
			textField.text = textField.text+="w";
		}
		
		public function onE(){
			textField.text = textField.text+="e";
		}
		
		public function onR(){
			textField.text = textField.text+="r";
		}
		
		public function onT(){
			textField.text = textField.text+="t";
		}

		public function onY(){
			textField.text = textField.text+="y";
		}
		
		public function onU(){
			textField.text = textField.text+="u";
		}
		
		public function onI(){
			textField.text = textField.text+="i";
		}
		
		public function onO(){
			textField.text = textField.text+="o";
		}
		
		public function onP(){
			textField.text = textField.text+="p";
		}
		
		public function onA(){
			textField.text = textField.text+="a";
		}
		
		public function onS(){
			textField.text = textField.text+="s";
		}
		
		public function onD(){
			textField.text = textField.text+="d";
		}
		
		public function onF(){
			textField.text = textField.text+="f";
		}
		
		public function onG(){
			textField.text = textField.text+="g";
		}
		
		public function onH(){
			textField.text = textField.text+="h";
		}
		
		public function onJ(){
			textField.text = textField.text+="j";
		}
		
		public function onK(){
			textField.text = textField.text+="k";
		}
		
		public function onL(){
			textField.text = textField.text+="l";
		}
		
		public function onZ(){
			textField.text = textField.text+="z";
		}
		
		public function onX(){
			textField.text = textField.text+="x";
		}
		
		public function onC(){
			textField.text = textField.text+="c";
		}
		
		public function onV(){
			textField.text = textField.text+="v";
		}
		
		public function onB(){
			textField.text = textField.text+="b";
		}
		
		public function onN(){
			textField.text = textField.text+="n";
		}
		
		public function onM(){
			textField.text = textField.text+="m";
		}
		
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}