﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	
	
	public class Menu extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
	    private var title:Title;
		private var instructions_BTN:Instuctions_BTN;
		private var play_BTN:Play_BTN;
		
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background(Background.MENU);
			addChild(background);
			
			
			
			play_BTN = new Play_BTN();
			play_BTN.x=750;
			play_BTN.y=270;
			addChild(play_BTN);
			instructions_BTN = new Instuctions_BTN();
			instructions_BTN.x=450;
			instructions_BTN.y=480;
			addChild(instructions_BTN);
			
			play_BTN.addEventListener(MouseEvent.CLICK, onPlay);
			instructions_BTN.addEventListener(MouseEvent.CLICK, onInstructions);
	
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onInstructions(event:Event):void
		{
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			background.removeFromParent();
			background = null;
			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}