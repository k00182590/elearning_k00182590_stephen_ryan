﻿package objects
{
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import states.Menu;
	
	
	public class Background extends Sprite
	{
		private var menu:MainMenu;
		private var gamePlay:Game_Play;
		private var gameOver:Game_Over;
		
		
		public static const MENU: int = 1;
		public static const GAMEPLAY: int = 2;
		public static const GAMEOVER: int =3;
		
		public function Background(i:int)
		{
			trace("i = " + i);
			switch(i) {
				case 1 : 
					menu = new MainMenu();
				addChild(menu);
				break;
				case 2 : 
					gamePlay = new Game_Play();
				addChild(gamePlay);
				break;
				case 3 : 
					gameOver = new Game_Over();
				addChild(gameOver);
				break;
}			
		}
		public function update():void
		{
			
		}
	public function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}